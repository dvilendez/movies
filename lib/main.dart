import 'package:flutter/material.dart';
 
import 'package:moovies/src/pages/home_page.dart';
import 'package:moovies/src/pages/moovie_detail.dart';

void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Películas',
      initialRoute: '/',
      routes: {
        '/': (BuildContext context) => HomePage(),
        'detail': (BuildContext context) => MoovieDetail(),
      },
    );
  }
}