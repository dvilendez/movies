
import 'package:http/http.dart' as http;

import 'dart:convert';
import 'dart:async';

import 'package:moovies/src/models/moovie_model.dart';
import 'package:moovies/src/models/actor_model.dart';

class MooviesProvider {
  String _apikey   = 'c8720ea9ed04c598869ef9d7cbcd5f5b';
  String _url      = 'api.themoviedb.org';
  String _language = 'es-ES';

  int _popularPage = 0;
  bool _loading = false;

  List<Moovie> _populars = new List();

  final _popularsStreamController = StreamController<List<Moovie>>.broadcast();

  Function(List<Moovie>) get popularsSink => _popularsStreamController.sink.add;

  Stream<List<Moovie>> get popularsStream => _popularsStreamController.stream;

  void disposeStreams() {
    _popularsStreamController?.close();
  }

  Future<List<Moovie>> _processResponse (Uri url) async {
    final response = await http.get(url);
    final decodedData = json.decode(response.body);
    final moovies = new Moovies.fromJsonList(decodedData['results']);

    return moovies.items;
  }


  Future<List<Moovie>> getNowPlaying() async {
    final url = Uri.https(_url, '3/movie/now_playing', {
      'api_key' : _apikey,
      'language': _language
    });
    return await _processResponse(url);
  }

  Future<List<Moovie>> getPopulars() async {

    if ( _loading) return [];

    _loading = true;

    _popularPage++;

    final url = Uri.https(_url, '3/movie/popular', {
      'api_key' : _apikey,
      'language': _language,
      'page'    : _popularPage.toString()
    });

    final response = await _processResponse(url);

    _populars.addAll(response);
    popularsSink(_populars);
    _loading = false;

    return response;
  }

  Future<List<Actor>> getCast(String moovieId) async {
    final url = Uri.https(_url, '3/movie/$moovieId/credits', {
      'api_key' : _apikey
    });

    final response = await http.get(url);
    final decodedData = json.decode(response.body);

    final cast = Cast.fromJsonList(decodedData['cast']);

    return cast.actors;
  }

  Future<List<Moovie>> searchMovie(String query) async {
    final url = Uri.https(_url, '3/search/movie', {
      'api_key' : _apikey,
      'language': _language,
      'query'   : query
    });
    return await _processResponse(url);
  }

}