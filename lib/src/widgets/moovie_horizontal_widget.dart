import 'package:flutter/material.dart';

import 'package:moovies/src/models/moovie_model.dart';

class MoovieHorizontal extends StatelessWidget {

  final List<Moovie> moovies;
  final Function nextPage;

  MoovieHorizontal({@required this.moovies, @required this.nextPage});

  final _pageController = new PageController(
    initialPage: 1,
    viewportFraction: 0.3
  );

  @override
  Widget build(BuildContext context) {
    
    final _screenSize = MediaQuery.of(context).size;

    _pageController.addListener(() {
      if (_pageController.position.pixels >= _pageController.position.maxScrollExtent - 200) {
        nextPage();
      }
    });

    return Container(
      height: _screenSize.height * 0.3,
      child: PageView.builder(
        pageSnapping: false,
        controller: _pageController,
        // children: _cards(context),
        itemCount: moovies.length,
        itemBuilder: (BuildContext context, int index) => _card(context, moovies[index]),
      ),
    );
  }

  Widget _card(BuildContext context, Moovie moovie) {

    moovie.uniqueId = '${moovie.id}-poster';

    final card = Container(
      margin: EdgeInsets.only(right: 15.0),
      child: Column(
        children: <Widget>[
          Hero(
            tag: moovie.uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: FadeInImage(
                image: NetworkImage(moovie.getPosterImg()),
                placeholder: AssetImage('assets/img/no-image.jpg'),
                fit: BoxFit.cover,
                height: 160.0,
              ),
            ),
          ),
          SizedBox(height: 5.0,),
          Text(
            moovie.title,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.caption,
          )
        ],
      ),
    );

    return GestureDetector(
      child: card,
      onTap: () {
        Navigator.pushNamed(context, 'detail', arguments: moovie);
      },
    );
  }

  List<Widget> _cards (BuildContext context) {
    return moovies.map((moovie) {
      return _card(context, moovie);
    }).toList();
  }
}