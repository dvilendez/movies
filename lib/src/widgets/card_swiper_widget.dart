import 'package:flutter/material.dart';

import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:moovies/src/models/moovie_model.dart';

class CardSwiper extends StatelessWidget {

  final List<Moovie> moovies;

  CardSwiper({@required this.moovies});


  @override
  Widget build(BuildContext context) {

    final _screenSize = MediaQuery.of(context).size;
    

    return Container(
      padding: EdgeInsets.only(top: 10.0),
      child: new Swiper(
        layout: SwiperLayout.STACK,
        itemWidth: _screenSize.width * 0.7,
        itemHeight: _screenSize.height * 0.5,
        itemBuilder: (BuildContext context,int index){

          moovies[index].uniqueId = '${moovies[index].id}-swiper';

          return Hero(
            tag: moovies[index].uniqueId,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'detail', arguments: moovies[index]),
                child: FadeInImage(
                  image: NetworkImage(moovies[index].getPosterImg()),
                  placeholder: AssetImage('assets/img/no-image.jpg'),
                  fit: BoxFit.cover,
                ),
              )
              // child: Text(moovies[index].toString()),
            ),
          );
        },
        itemCount: moovies.length,
        // pagination: new SwiperPagination(),
        // control: new SwiperControl(),
      ),
    );
  }
}